FROM ubuntu:18.04

ENV DEBIAN_FRONTEND noninteractive
ENV container docker
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8

RUN apt -qq update && \
  apt -qq -y install git \
  netcat \
  netbase \
  openssh-server \
  python3-minimal \
  python3-setuptools \
  python3-pip \
  python3-dev \
  python3-dateutil \
  ca-certificates \
  openstack-pkg-tools \
  apache2 \
  libmysqlclient-dev \
  gcc \
  g++ \
  libffi-dev \
  libssl-dev --no-install-recommends && \
  apt-get clean && \
  rm -rf \
     /var/lib/apt/lists/* \
     /tmp/* \
     /var/tmp/* \
     /usr/share/man \
     /usr/share/doc \
     /usr/share/doc-base

RUN ln -s /usr/bin/python3 /usr/bin/python && \
  pip3 install -U setuptools && \
  pip3 install wheel && \
  pip3 install pbr==2.0.0


##########################################################################
### aic-orm-tempest-plugin  setup
##########################################################################
WORKDIR /
COPY . ranger-tempest-plugin/

WORKDIR /ranger-tempest-plugin/
RUN python3 setup.py develop

##########################################################################
### END OF ranger-tempest-plugin setup
##########################################################################

##########################################################################
### openstack tempest setup steps
##########################################################################

### reset workdir to root before executing tempest steps
WORKDIR /

### create required tempest directories - and remove .stestr folder
RUN mkdir -p /tempest/logs \
    && mkdir -p /etc/tempest \
    && mkdir -p /tempest/tempest_lock \
    && mkdir -p /tempest/images \
    && mkdir -p /var/log/tempest

# copy tempest test setup files
COPY tempest_setup/tempest.conf  /etc/tempest

##########################################################################
### END OF  openstack tempest setup steps
##########################################################################

##########################################################################
### RUN tempest tests on test_regions
##########################################################################
### create egg-info for tempest

WORKDIR /ranger-tempest-plugin
#ENTRYPOINT stestr run ranger_tempest_plugin.tests.api.test_regions \
#   && /bin/bash
#ENTRYPOINT stestr run ranger_tempest_plugin.tests.api.test_flavors/ \
#   && /bin/bash
#ENTRYPOINT stestr run ranger_tempest_plugin.tests.api.test_customers/ \
#   && /bin/bash
#ENTRYPOINT stestr run ranger_tempest_plugin.tests.api.test_images/ \
#   && /bin/bash


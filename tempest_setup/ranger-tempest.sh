#!/bin/bash
# Shell script to move to tempest, copy tempest.conf, and
#   initialize tempest tests using stestr
# It should be noted that this script can not be ran out of
#   the box and does require configuration before running.

function execute_tests {
    # move to Tempest directory
    cd ${TEMPEST_DIRECTORY}

    # check for necessary folders, make them if not found
    if [[ ! -d tempest_lock ]]; then
        mkdir tempest_lock
    fi
    if [[ ! -d images ]]; then
        mkdir images
    fi
    if [[ ! -d .stestr ]]; then
        stestr init
    fi

    # sets PLUGIN_DIRECTORY to relevant subdirectory and copies files for tests
    PLUGIN_DIRECTORY=${PLUGIN_DIRECTORY}/tempest_setup

    # check for necessary files, copy them from ranger if not found
    if [[ ! -e ./.stestr.conf ]]; then
        cp ${PLUGIN_DIRECTORY}/.stestr.conf ./
    fi
    if [[ ! -e etc/tempest.conf ]]; then
        cp ${PLUGIN_DIRECTORY}/tempest.conf etc/
    fi
    if [[ ! -e etc/create_tenant.sh ]]; then
        cp ${PLUGIN_DIRECTORY}/create_tenant.sh etc/
    fi
    if [[ ! -e etc/accounts.yaml ]]; then
        cp ${PLUGIN_DIRECTORY}/accounts.yaml etc/
    fi

    # runs tests using stestr and regex, ex: ranger_tempest_plugin.tests.api.test_regions
    stestr run --concurrency ${CONCURRENCY} --log-file /var/log/tempest/tempest_run.log ${TEST_REGEX}
}

usage()
{
cat << EOF
usage: ./ranger-tempest.sh -t TEMPEST_DIRECTORY -c CONCURRENCY -r PLUGIN_DIRECTORY -f TEST_REGEX

This script automates a few steps necessary to run Tempest against Ranger

OPTIONS:
   -h      Show this message
   -t      The Tempest Folder fully-formed path
   -c      Concurrency
   -r      The location of your Ranger folder
   -f      The regex representing the tests that will be ran if you do not wish to run all tests
EOF
}

TEMPEST_DIRECTORY=
CONCURRENCY=1
TEST_REGEX=
PLUGIN_DIRECTORY=
while getopts "ht:c:f:r:" OPTION
do
     case $OPTION in
        h)
            usage
            exit 1
            ;;
        t)
            TEMPEST_DIRECTORY=$OPTARG
            ;;
        c)
            CONCURRENCY=$OPTARG
            ;;
        f)
            TEST_REGEX=$OPTARG
            ;;
        r)
            PLUGIN_DIRECTORY=$OPTARG
            ;;
        ?)
            usage
            exit
            ;;
     esac
done

if [[ -z $TEMPEST_DIRECTORY ]]; then
    echo "The script requires the location of the Tempest folder"
    usage
    exit 1
#elif [[ -z $TEST_REGEX ]]; then
#    echo "The script expects a regex of tests to run"
#    usage
#    exit 1
elif [[ -z $PLUGIN_DIRECTORY ]]; then
    echo "This script requires the location of the ranger-tempest-plugin folder"
    usage
    exit 1
else
    execute_tests
fi


# The order of packages is significant, because pip processes them in the order
# of appearance. Changing the order has an impact on the overall integration
# process, which may cause wedges in the gate later.

flake8>=3.8
coverage>=4.0,!=4.4 # Apache-2.0
oslo.config>=5.2.0 # Apache-2.0
oslo.i18n>=3.15.3 # Apache-2.0
oslo.log>=3.36.0 # Apache-2.0
oslosphinx>=4.7.0 # Apache-2.0
oslotest>=1.10.0 # Apache-2.0
oslo.utils>=3.33.0 # Apache-2.0
pylint >= 2.5.3
python-subunit>=0.0.18 # Apache-2.0/BSD
reno>=1.8.0 # Apache-2.0
requests>=2.10.0
testfixtures>=5.1.1 # MIT
stestr>=2.0.0
tempest>=19.0.0
testtools>=1.4.0 # MIT

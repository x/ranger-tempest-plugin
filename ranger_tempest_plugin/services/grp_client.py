# Copyright 2016 AT&T Corp
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the 'License'); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an 'AS IS' BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
#
#    Group processes currently are non-functional, but this code is being
#    kept for the time being until we are certain it is no longer needed.

import json
import urllib

from ranger_tempest_plugin.schemas import group_schema as schema
from ranger_tempest_plugin.services import base_client

from tempest import config

CONF = config.CONF


class GrpClient(base_client.RangerClientBase):

    cms_url = CONF.ranger.ranger_cms_base_url
    version = 'v1'

    def create_group(self, **kwargs):
        uri = '%s/%s/orm/groups' % (self.cms_url, self.version)
        post_body = json.dumps(kwargs)
        return self.post_request(uri, post_body, schema.create_group)

    def get_group(self, identifier):
        uri = '%s/%s/orm/groups/%s' \
            % (self.cms_url, self.version, identifier)
        return self.get_request(uri, schema.get_group)

    def list_groups(self, filter=None):
        uri = '%s/%s/orm/groups' % (self.cms_url, self.version)
        if filter is not None:
            uri += '?' + urllib.parse.urlencode(filter)
        return self.get_request(uri, schema.list_groups)

    def add_groups_region(self, group_id, *args):
        uri = '%s/%s/orm/groups/%s/regions' % (
            self.cms_url, self.version, group_id)
        post_body = json.dumps(args)
        return self.post_request(uri, post_body, schema.add_groups_region)

    def delete_groups_region(self, group_id, region_id):
        uri = '%s/%s/orm/groups/%s/regions/%s' % (
            self.cms_url, self.version, group_id, region_id)
        return self.delete_request(uri, schema.delete_groups_region)

    def delete_group(self, group_id):
        uri = '%s/%s/orm/groups/%s' \
            % (self.cms_url, self.version, group_id)
        return self.delete_request(uri, schema.delete_group)

    def assign_group_roles(self, group_id, *args):
        uri = '%s/%s/orm/groups/%s/roles' % (
            self.cms_url, self.version, group_id)
        post_body = json.dumps(args)
        return self.post_request(uri, post_body, schema.assign_group_roles)

    def assign_group_region_roles(self, group_id, region_id, *args):
        uri = '%s/%s/orm/groups/%s/regions/%s/roles' % (
            self.cms_url, self.version, group_id, region_id)
        post_body = json.dumps(args)
        return self.post_request(uri, post_body, schema.assign_group_roles)

    def unassign_group_role(
            self, group_id, role, assignmenet_type, assignment_value):
        uri = '%s/%s/orm/groups/%s/roles/%s/%s/%s' % (self.cms_url,
                                                      self.version,
                                                      group_id,
                                                      role,
                                                      assignmenet_type,
                                                      assignment_value)
        return self.delete_request(uri, schema.unassign_group_role)

    def list_group_roles(self, group_id, params):
        uri = '%s/%s/orm/groups/%s/roles/%s' % (
            self.cms_url, self.version, group_id, params)
        return self.get_request(uri, schema.list_group_roles)

    def add_group_default_user(self, group_id, *args):
        uri = '%s/%s/orm/groups/%s/users' \
            % (self.cms_url, self.version, group_id)
        post_body = json.dumps(args)
        return self.post_request(uri, post_body, schema.add_groups_users)

    def delete_group_default_user(self, group_id, user_id, user_domain):
        uri = '%s/%s/orm/groups/%s/users/%s/%s' % (
            self.cms_url, self.version, group_id, user_id, user_domain)
        return self.delete_request(uri, schema.delete_groups_region)

    def add_group_region_user(self, group_id, region_id, *args):
        uri = '%s/%s/orm/groups/%s/regions/%s/users' \
            % (self.cms_url, self.version, group_id, region_id)
        post_body = json.dumps(args)
        return self.post_request(uri, post_body, schema.add_groups_users)

    def delete_groups_region_user(self, group_id, region_id,
                                  user_id, user_domain):
        uri = '%s/%s/orm/groups/%s/regions/%s/users/%s/%s' % (
            self.cms_url, self.version, group_id, region_id,
            user_id, user_domain)
        return self.delete_request(uri, schema.delete_groups_region)

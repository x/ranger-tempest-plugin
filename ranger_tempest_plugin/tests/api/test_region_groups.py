# Copyright 2016 AT&T Corp
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from ranger_tempest_plugin import data_utils as orm_data_utils
from ranger_tempest_plugin.tests.api import base
from ranger_tempest_plugin.tests.api.rms_base import RmsBaseOrmTest

from tempest.lib import decorators
from tempest.lib import exceptions


class TestTempestRegionGroup(base.BaseOrmTest):

    @classmethod
    def resource_setup(cls):
        super(TestTempestRegionGroup, cls).resource_setup()

        cls.region_ids = []
        # create standard region
        _, cls.region_1 = cls.client.create_region(
            **RmsBaseOrmTest._get_region_params())
        cls.region_ids.append(cls.region_1['id'])

        # create region sharing region_1 properties
        another_region = RmsBaseOrmTest._get_region_params()
        another_region['metadata'] = {'key2': ['value2']}
        _, cls.region_2 = cls.client.create_region(**another_region)
        cls.region_ids.append(cls.region_2['id'])

        for region_id in cls.region_ids:
            cls.addClassResourceCleanup(cls.client.delete_region, region_id)

        # set up region groups
        cls.group_ids = []
        _, cls.group_1 = cls.client.create_region_group(
            **orm_data_utils.rand_region_group(cls.region_ids))
        cls.group_ids.append(cls.group_1['group']['id'])

        _, cls.group_2 = cls.client.create_region_group(
            **orm_data_utils.rand_region_group(cls.region_ids))
        cls.group_ids.append(cls.group_2['group']['id'])

        for group_id in cls.group_ids:
            cls.addClassResourceCleanup(cls.client.delete_region_group,
                                        group_id)

    @classmethod
    def setup_clients(cls):
        super(TestTempestRegionGroup, cls).setup_clients()
        cls.client = cls.os_primary.rms_client

    @decorators.idempotent_id('0d377eb2-754d-49c1-9a4f-c7019dfe80ca')
    def test_update_group(self):
        _id = self.group_ids[-1]
        group = orm_data_utils.rand_region_group(self.region_ids, _id)
        _, body = self.client.update_region_group(_id, **group)
        self.assert_expected(group, body['group'], ['regions'])

    @decorators.idempotent_id('b946c6c4-d601-42b9-befd-ba40992a3c53')
    def test_list_groups(self):
        _, body = self.client.list_region_groups()
        groups = [x['id'] for x in body['groups']]
        self.assertIn(self.group_1['group']['id'], groups)
        self.assertIn(self.group_2['group']['id'], groups)

    @decorators.idempotent_id('9a37d966-4416-4ff3-8f3b-6847810662d7')
    def test_get_group(self):
        _id = self.group_1['group']['id']
        _, body = self.client.get_region_group(_id)
        self.assert_expected(self.group_1['group'], body, ['links'])

    @decorators.idempotent_id('504cc5cb-bc19-48ea-9456-16f5b24c9529')
    def test_create_region_group(self):
        _, region_group = self.client.create_region_group(
            **orm_data_utils.rand_region_group(self.region_ids)
        )
        _, region_group_details = self.client.get_region_group(
            region_group['group']['id']
        )
        self.assertIn(
            self.region_ids[0],
            region_group_details['regions']
        )
        self.assertIn(
            self.region_ids[1],
            region_group_details['regions']
        )

        self.addCleanup(
            self.client.delete_region_group,
            region_group['group']['id'])

    @decorators.idempotent_id('504cc5cb-bc19-48ea-9456-16f5b24c9530')
    def test_delete_region_group(self):
        _, region_group = self.client.create_region_group(
            **orm_data_utils.rand_region_group(self.region_ids)
        )
        _, region_group_details = self.client.get_region_group(
            region_group['group']['id']
        )

        self.assertIn(
            self.region_ids[0],
            region_group_details['regions']
        )
        self.assertIn(
            self.region_ids[1],
            region_group_details['regions']
        )

        self.client.delete_region_group(region_group['group']['id'])
        self.assertRaises(
            exceptions.NotFound, self.client.get_region_group,
            region_group['group']['id']
        )

    @decorators.idempotent_id('504cc5cb-bc19-48ea-9456-16f5b24c9531')
    def test_validate_region_group(self):
        # Validate the Regions added to group are present in Ranger
        # try to add NON-EXISTENANT Region in the payload to create Group
        ids = ['some_non_existenant_region']

        self.assertRaises(
            exceptions.NotFound, self.client.create_region_group,
            **orm_data_utils.rand_region_group(ids)
        )

# Copyright 2016 AT&T Corp
# All Rights Reserved
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from ranger_tempest_plugin import data_utils as orm_data_utils
from ranger_tempest_plugin.tests.api import rms_base

from tempest.lib import decorators
from tempest.lib import exceptions


class TestTempestRegion(rms_base.RmsBaseOrmTest):

    def _list_regions_with_filter(self, filter, key):
        _, body = self.client.list_regions(filter)
        regions = body['regions']
        self.assertTrue(
            all([region[key] == self.region_1[key] for region in regions]))

    @decorators.idempotent_id('829c7da0-2332-4f80-ad35-24306b67ed0e')
    def test_create_and_delete_region(self):
        # create new region for API test
        region = orm_data_utils.rand_region()
        _, region_body = self.client.create_region(**region)

        test_region_name = region_body['name']
        test_region_id = region_body['id']
        _, body = self.client.get_region(test_region_name)
        self.assertEqual(test_region_id, body['id'])

        # now delete the region
        self.client.delete_region(test_region_id)
        self.assertRaises(exceptions.NotFound, self.client.get_region,
                          test_region_id)

    @decorators.idempotent_id('eecedcb0-9c96-453d-bd72-71dba26fa1c5')
    def test_list_region(self):
        _, body = self.client.list_regions()
        regions = [x['id'] for x in body['regions']]
        self.assertIn(self.region_1['id'], regions)

    @decorators.idempotent_id('0164e040-7775-4837-9ac2-aaa0f71cdfca')
    def test_list_region_v1(self):
        _, body = self.client.list_regions_v1()
        regions = [x['id'] for x in body]
        self.assertIn(self.region_1['id'], regions)

    @decorators.idempotent_id('e6c6fdfe-5fa2-45f5-8bd8-fb9cf79e99fc')
    def test_list_region_with_name(self):
        filter = {'regionname': self.region_1['name']}
        self._list_regions_with_filter(filter, 'name')

    @decorators.idempotent_id('b3310e32-0c31-4d37-9789-cc3c4639dabe')
    def test_list_region_with_osversion(self):
        value = self.region_1['OSVersion']
        filter = {'osversion': value}
        _, body = self.client.list_regions(filter)
        regions = body['regions']
        self.assertTrue(
            all(region['OSVersion'].lower()
                == self.region_1['OSVersion'].lower() for region in regions))

    @decorators.idempotent_id('0b2d3e79-c14a-4527-94b0-04eeae053a80')
    def test_list_region_with_status(self):
        filter = {'status': self.region_1['status']}
        self._list_regions_with_filter(filter, 'status')

    @decorators.idempotent_id('871be582-ecaa-4a46-a403-4d6b5e59d7de')
    def test_list_region_with_ranger_version(self):
        filter = {'ranger_agent_version': self.region_1['rangerAgentVersion']}
        self._list_regions_with_filter(filter, 'rangerAgentVersion')

    @decorators.idempotent_id('ac18be48-c787-4a65-913f-a0b0a80fbd1d')
    def test_list_region_with_clli(self):
        filter = {'clli': self.region_1['CLLI']}
        self._list_regions_with_filter(filter, 'CLLI')

    @decorators.idempotent_id('f2b2361d-ce71-43a8-9f01-acb529835880')
    def test_list_region_with_metadata(self):
        filter = {'metadata': list(self.region_2['metadata'].keys())[0]}
        _, body = self.client.list_regions(filter)
        regions = body['regions']
        for r in regions:
            self.assertIn(filter['metadata'], r['metadata'].keys())

    @decorators.idempotent_id('4533b31a-115d-466d-bf75-8ac24338c1a5')
    def test_list_region_with_address(self):
        filter = {
            'country': self.region_1['address']['country'],
            'state': self.region_1['address']['state'],
            'city': self.region_1['address']['city'],
            'street': self.region_1['address']['street'],
            'zip': self.region_1['address']['zip']
        }
        self._list_regions_with_filter(filter, 'address')

    @decorators.idempotent_id('4235a73b-2437-4466-8af8-5d29da2cd236')
    def test_list_region_by_address_parameters(self):
        random_region = orm_data_utils.rand_region()
        random_region['address']['country'] = 'ABC'
        random_region['address']['state'] = 'XX'
        random_region['address']['city'] = 'YYY'
        random_region['address']['street'] = '123 Test Street'
        random_region['address']['zip'] = '11211'
        _, region_body = self.client.create_region(**random_region)
        self.addCleanup(self.client.delete_region, region_body['id'])

        # Filter by country
        filter = {'country': region_body['address']['country']}
        _, body = self.client.list_regions(filter)
        regions = body['regions']
        self.assertTrue(all(
            [region['address']['country'] == region_body['address']['country']
                for region in regions]))

        # Filter by state
        filter = {'state': region_body['address']['state']}
        _, body = self.client.list_regions(filter)
        regions = body['regions']
        self.assertTrue(all(
            [region['address']['state'] == region_body['address']['state']
                for region in regions]))

        # Filter by city
        filter = {'city': region_body['address']['city']}
        _, body = self.client.list_regions(filter)
        regions = body['regions']
        self.assertTrue(all(
            [region['address']['city'] == region_body['address']['city']
                for region in regions]))

        # Filter by street
        filter = {'street': region_body['address']['street']}
        _, body = self.client.list_regions(filter)
        regions = body['regions']
        self.assertTrue(all(
            [region['address']['street'] == region_body['address']['street']
                for region in regions]))

        # Filter by zip
        filter = {'zip': region_body['address']['zip']}
        _, body = self.client.list_regions(filter)
        regions = body['regions']
        self.assertTrue(all(
            [region['address']['zip'] == region_body['address']['zip']
                for region in regions]))

    @decorators.idempotent_id('b40fa67c-94ab-4b33-8441-70f50b50a17e')
    def test_list_region_with_location_type(self):
        filter = {'location_type': self.region_1['locationType']}
        self._list_regions_with_filter(filter, 'locationType')

    @decorators.idempotent_id('726b8215-af10-4385-83c7-32b51502dff1')
    def test_list_region_with_type(self):
        filter = {'type': self.region_1['designType']}
        self._list_regions_with_filter(filter, 'designType')

    @decorators.idempotent_id('4875ea70-a5a1-4b46-b752-246221670d26')
    def test_list_region_with_vlcp(self):
        filter = {'vlcp_name': self.region_1['vlcpName']}
        self._list_regions_with_filter(filter, 'vlcpName')

    @decorators.idempotent_id('358f3cbc-4ae5-4b43-be36-6df55eae8fd9')
    def test_get_region(self):
        _, body = self.client.get_region(self.region_2['id'])
        self.assert_expected(self.region_2, body, [])

    @decorators.idempotent_id('cefb952f-7777-4878-87d2-d78ac345f0d2')
    def test_get_region_metadata(self):
        _, body = self.client.get_region_metadata(self.region_2['id'])
        self.assert_expected({'metadata': {'key2': ['value2']}}, body, [])

    @decorators.idempotent_id('b2c3baf5-22af-4bf9-bcad-b6a1a74e82d9')
    def test_update_region(self):
        _id = self.setup_ids[-1]
        region = orm_data_utils.rand_region(_id)
        _, body = self.client.update_region(_id, **region)
        self.assert_expected(region, body, [])

    @decorators.idempotent_id('0d5644d8-92bc-497c-8fc5-b57417d86e6d')
    def test_update_region_status(self):
        status = {}
        status['status'] = orm_data_utils.rand_region_status(
            [self.region_1['status']])
        _, body = self.client.update_region_status(self.region_1['id'], status)
        self.assert_expected(status, body, ['links'])
        status['status'] = 'functional'
        _, body = self.client.update_region_status(self.region_1['id'], status)

    @decorators.idempotent_id('5c1a2624-6abe-49e7-82c8-30e8df1377d0')
    def test_update_region_metadata(self):
        metadata = {}
        metadata['metadata'] = orm_data_utils.rand_region_metadata()
        _, body = self.client.update_region_metadata(self.region_1['id'],
                                                     metadata)
        self.assert_expected(metadata, body, [])

    @decorators.idempotent_id('77257e0c-e2f8-4b98-886c-359508a4a73d')
    def test_list_multiple_filter(self):
        filter = {
            'vlcp_name': self.region_1['vlcpName'],
            'status': self.region_1['status'],
            'regionname': self.region_1['name']
        }
        self._list_regions_with_filter(filter, 'name')

    @decorators.idempotent_id('4a8975f0-c9fd-477c-a01d-5679552083f3')
    def test_add_delete_region_metadata(self):
        # get original metadata before addition
        _, region = self.client.get_region(self.region_2['id'])
        orig_metadata = region['metadata']

        # add metadata
        metadata = {'metadata': {'m1': ['km1']}}
        self.client.add_region_metadata(self.region_2['id'], **metadata)
        _, body = self.client.get_region(self.region_2['id'])

        new_metadata = dict(
            list(orig_metadata.items()) + list(metadata['metadata'].items()))

        self.assertDictEqual(body['metadata'], new_metadata)

        # remove metadata
        self.client.delete_region_metadata(self.region_2['id'], 'm1')
        _, body = self.client.get_region(self.region_2['id'])
        self.assertDictEqual(body['metadata'], orig_metadata)

    @decorators.idempotent_id('75c2e596-4ea9-454f-94a2-e5d084af2059')
    def test_delete_region_with_resource(self):
        # delete region when a customer is assigned a region
        self.assertRaises(exceptions.Conflict,
                          self.client.delete_region,
                          self.setup_customer['regions'][0]['name'])

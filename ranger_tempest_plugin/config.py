# Copyright 2015
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from oslo_config import cfg


OSLO_CONCURRENCY_GROUP = cfg.OptGroup(
    name="oslo_concurrency",
    title="test concurrency configuration options"
)

CONCURRENCY_GROUP = [
    cfg.StrOpt("lock_path", default='/tempest/lock_path',
               help="Lock file location for external locks")
]

SERVICE_AVAILABLE_GROUP = cfg.OptGroup(
    name="service_available",
    title="Available OpenStack Services"
)

ORM_SERVICE_AVAILABLE_GROUP = [
    cfg.BoolOpt("ranger", default=False,
                help="Whether or not ranger is expected to be available")
]

ORM_GROUP = cfg.OptGroup(
    name="ranger",
    title="Ranger Service option"
)

ORM_GROUP_OPTIONS = [
    cfg.StrOpt("image_url",
               default='',
               help="swift container url where image is located"),
    cfg.StrOpt("ranger_cms_base_url",
               help="Ranger Project Service URL"),
    cfg.StrOpt("ranger_fms_base_url",
               help="Ranger Flavor Service URL"),
    cfg.StrOpt("ranger_ims_base_url",
               help="Ranger Image Service URL"),
    cfg.StrOpt("ranger_rms_base_url",
               help="Ranger Region Service URL"),
    cfg.BoolOpt('verify',
                default=False,
                help='Flag for SSL verfiy Enabled/Disabled.'),
    cfg.BoolOpt('auth_enabled',
                default=False,
                help='Token Authentication enabled/disabled'),
    cfg.ListOpt("flavor_series",
                default=['xx'],
                help="Supported flavor series"),
    cfg.StrOpt("catalog_type",
               default='ranger',
               help="ranger catalog type"),
    cfg.BoolOpt("alt_region_available",
                default=False,
                help="Alt region for ranger tempest testing"),
    cfg.StrOpt("alt_region",
               default='',
               help="Provide alternate region for creating"
               "resources with multiple regions"),
    cfg.StrOpt("test_rbac",
               default=False,
               help="Determines if rbac tests should be ran")
]
